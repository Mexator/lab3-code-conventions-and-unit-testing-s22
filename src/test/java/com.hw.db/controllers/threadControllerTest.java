package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class threadControllerTest {

    @Test
    void checkIdOrSlugTest() {
        threadController controller = new threadController();

        Thread testThread = Mockito.mock(Thread.class);

        MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class);
        threadDAO.when(() -> ThreadDAO.getThreadById(Mockito.any(Integer.class)))
                .thenReturn(testThread);
        threadDAO.when(() -> ThreadDAO.getThreadBySlug(Mockito.any(String.class)))
                .thenReturn(testThread);

        controller.CheckIdOrSlug("0");
        threadDAO.verify(() -> ThreadDAO.getThreadById(0));

        controller.CheckIdOrSlug("132144234");
        threadDAO.verify(() -> ThreadDAO.getThreadById(132144234));

        controller.CheckIdOrSlug("test");
        threadDAO.verify(() -> ThreadDAO.getThreadBySlug("test"));

        controller.CheckIdOrSlug("-1.0");
        threadDAO.verify(() -> ThreadDAO.getThreadBySlug("-1.0"));

        threadDAO.close();
    }

    @Test
    void correctCreatePostTest() {
        Post post = new Post("user1",
                Timestamp.from(Instant.now()),
                "forum",
                "message",
                0,
                0,
                false);
        User user = Mockito.mock(User.class);
        Mockito.when(user.getNickname()).thenReturn("nick");

        Thread testThread = Mockito.mock(Thread.class);
        Mockito.when(testThread.getForum()).thenReturn("f");
        Mockito.when(testThread.getId()).thenReturn(123);

        MockedStatic<UserDAO> userDao = Mockito.mockStatic(UserDAO.class);
        userDao.when(() -> UserDAO.Info(Mockito.any(String.class)))
                .thenReturn(user);
        MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class);
        threadDAO.when(() -> ThreadDAO.getThreadById(Mockito.any(Integer.class)))
                .thenReturn(testThread);
        threadDAO.when(() -> ThreadDAO.getThreadBySlug(Mockito.any(String.class)))
                .thenReturn(testThread);

        ResponseEntity ret = new threadController().createPost("test", List.of(post));

        assertEquals(HttpStatus.CREATED, ret.getStatusCode());
        assertEquals(List.of(post), ret.getBody());

        threadDAO.verify(() -> ThreadDAO.createPosts(
                testThread,
                List.of(post),
                List.of(user)));

        assertEquals("f", post.getForum());
        assertEquals(user.getNickname(), post.getAuthor());
        assertEquals(testThread.getId(), post.getThread());

        userDao.close();
        threadDAO.close();
    }

    @Test
    void correctPostsTest() {
        Post post = new Post("user1",
                Timestamp.from(Instant.now()),
                "forum",
                "message",
                0,
                0,
                false);

        Thread testThread = Mockito.mock(Thread.class);
        Mockito.when(testThread.getId()).thenReturn(123);

        MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class);
        threadDAO.when(() -> ThreadDAO.getThreadById(Mockito.any(Integer.class)))
                .thenReturn(testThread);
        threadDAO.when(() -> ThreadDAO.getThreadBySlug(Mockito.any(String.class)))
                .thenReturn(testThread);
        threadDAO.when(() -> ThreadDAO.getPosts(123, 0, 0, "", false))
                .thenReturn(List.of(post));


        ResponseEntity ret = new threadController().Posts("test",
                0,
                0,
                "",
                false);

        assertEquals(HttpStatus.OK, ret.getStatusCode());
        assertEquals(List.of(post), ret.getBody());

        threadDAO.verify(() -> ThreadDAO.getPosts(123, 0, 0, "", false));

        threadDAO.close();
    }

    @Test
    void correctChangeTest() {

        Thread testThread = Mockito.mock(Thread.class);

        MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class);
        threadDAO.when(() -> ThreadDAO.getThreadById(Mockito.any(Integer.class)))
                .thenReturn(testThread);
        threadDAO.when(() -> ThreadDAO.getThreadBySlug(Mockito.any(String.class)))
                .thenReturn(testThread);

        Thread newTh = Mockito.mock(Thread.class);
        ResponseEntity ret = new threadController().change("test", newTh);

        assertEquals(HttpStatus.OK, ret.getStatusCode());
        assertEquals(testThread, ret.getBody());

        threadDAO.verify(() -> ThreadDAO.change(testThread, newTh));

        threadDAO.close();
    }

    @Test
    void correctInfoTest() {

        Thread testThread = Mockito.mock(Thread.class);

        MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class);
        threadDAO.when(() -> ThreadDAO.getThreadById(Mockito.any(Integer.class)))
                .thenReturn(testThread);
        threadDAO.when(() -> ThreadDAO.getThreadBySlug(Mockito.any(String.class)))
                .thenReturn(testThread);

        ResponseEntity ret = new threadController().info("test");

        assertEquals(HttpStatus.OK, ret.getStatusCode());
        assertEquals(testThread, ret.getBody());

        threadDAO.close();
    }

    @Test
    void correctCreateVoteTest() {

        Thread testThread = Mockito.mock(Thread.class);
        Mockito.when(testThread.getId()).thenReturn(123);
        Mockito.when(testThread.getVotes()).thenReturn(12345);

        User user = Mockito.mock(User.class);
        Mockito.when(user.getNickname()).thenReturn("nick");

        MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class);
        threadDAO.when(() -> ThreadDAO.getThreadById(Mockito.any(Integer.class)))
                .thenReturn(testThread);
        threadDAO.when(() -> ThreadDAO.getThreadBySlug(Mockito.any(String.class)))
                .thenReturn(testThread);

        MockedStatic<UserDAO> userDao = Mockito.mockStatic(UserDAO.class);
        userDao.when(() -> UserDAO.Info(Mockito.any(String.class)))
                .thenReturn(user);

        Vote vote = Mockito.mock(Vote.class);
        Mockito.when(vote.getNickname()).thenReturn("nick");
        ResponseEntity ret = new threadController().createVote("test", vote);

        assertEquals(HttpStatus.OK, ret.getStatusCode());
        assertEquals(testThread, ret.getBody());

        Mockito.verify(vote).setTid(123);
        Mockito.verify(testThread).setVotes(12345);
        threadDAO.verify(() -> ThreadDAO.createVote(testThread, vote));

        userDao.close();
        threadDAO.close();
    }
}
